#!/usr/bin/env bash

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$CURRENT_DIR/../config/vars.sh"
source "$CURRENT_DIR/../lib/tmux.sh"

ring_a_bell() {
    echo -ne "\a"
}

main() {
  local target=$1
  local tmux_make_filename
  local window_id

  window_id="$(tmux:get_window_id)"

  if [ -n "$TMUX_MAKE_FILENAME" ];then
      tmux_make_filename=$TMUX_MAKE_FILENAME
  else
      tmux_make_filename=$(tmux:get_option 'makefile' 'Makefile')
  fi

  if ! make --file "$tmux_make_filename" "$target"; then
      tmux rename-window -t "$window_id" " make $target"
      ring_a_bell
  else
      tmux rename-window -t "$window_id" " make $target"
  fi

  read -r -p "Press any key to continue"
}


main "$@"

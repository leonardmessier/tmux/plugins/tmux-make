#!/usr/bin/env bash

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source "$CURRENT_DIR/../config/vars.sh"
source "$CURRENT_DIR/../lib/tmux.sh"

main() {
  if [ -n "$TMUX_MAKE_FILENAME" ];then
      tmux_make_filename=$TMUX_MAKE_FILENAME
  else
      tmux_make_filename=$(tmux:get_option 'makefile' 'Makefile')
  fi

  $EDITOR "$tmux_make_filename"
}

main "$@"


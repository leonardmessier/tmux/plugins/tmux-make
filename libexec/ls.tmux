#!/usr/bin/env bash
CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source "$CURRENT_DIR/../config/vars.sh"
source "$CURRENT_DIR/../lib/tmux.sh"

get_make_targets() {
  local current_pwd="$1"
  local tmux_make_filename

  if [ -n "$TMUX_MAKE_FILENAME" ];then
      tmux_make_filename=$TMUX_MAKE_FILENAME
  else
      tmux_make_filename=$(tmux:get_option 'makefile' 'Makefile')
  fi

  if ! [ -f "$tmux_make_filename" ]; then
      tmux:display_message "No Makefile found in $(dirname "$tmux_make_filename")" 2000

      return
  fi

  cat "$tmux_make_filename" | grep -E '^[a-zA-Z_-]+:.*?## .*$$' | awk 'BEGIN {FS=":"}; {printf "%s\n", $1}'
}

main() {
  local pane_id=$1
  local make_targets
  local header='enter=execute, ctrl-e=edit'
  local current_pwd

  current_pwd="$(tmux:get_pane_info "$pane_id" '#{pane_current_path}')"
  make_targets="$(get_make_targets "$current_pwd")"
  if [ -z "$make_targets" ]; then
      return
  fi

  if [ -n "$TMUX_MAKE_FILENAME" ];then
      tmux_make_filename=$TMUX_MAKE_FILENAME
  else
      tmux_make_filename=$(tmux:get_option 'makefile' 'Makefile')
  fi

  local read_target_doc
  read_target_doc="cat $tmux_make_filename | grep {} | awk 'BEGIN {FS=\"##\"}; {printf \"%s\n\", \$2}'"

  local chosen_target
  chosen_target="$(echo -e "$make_targets" | fzf --preview="$read_target_doc" --header="$header" --expect=enter,ctrl-e)"

  if [ $? -gt 0 ]; then
      exit 0
  fi

  local key
  local text
  key=$(head -1 <<< "$chosen_target")
  text=$(tail -n +2 <<< "$chosen_target")

  local cmd
  case $key in
    enter)
        cmd="$CURRENT_DIR/exec.tmux $text"
        tmux new-window -n " make $text" -a -c '#{pane_current_path}' "$cmd"
        ;;

    ctrl-e)
        cmd="$CURRENT_DIR/edit.tmux '#{pane_id}'"
        tmux display-popup -E -w="75%" -h="75%" "$cmd"
        ;;
  esac
}

main "$@"

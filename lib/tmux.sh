#!/usr/bin/env bash

tmux:get_option() {
  local option=$1
  local default_value=$2
  local option_value

  option_value=$(tmux show-option -gqv "$option")
  if [ -z "$option_value" ]; then
    echo "$default_value"
  else
    echo "$option_value"
  fi
}

tmux:set_option() {
  local option=$1
  local value=$2
  tmux set-option -gq "$option" "$value"
}

# @see https://github.com/tmux-plugins/tmux-sidebar/blob/master/scripts/helpers.sh
tmux:get_pane_info() {
  local pane_id="$1"
  local format_strings="#{pane_id},$2"
  tmux list-panes -t "$pane_id" -F "$format_strings" |
    \grep "$pane_id" |
    cut -d',' -f2-
}

tmux:get_window_id() {
    tmux lsw -F '#{window_id}#{window_active}'|sed -n 's|^\(.*\)1$|\1|p'
}

tmux:display_message() {
  local message="$1"

  # display_duration defaults to 5 seconds, if not passed as an argument
  if [ "$#" -eq 2 ]; then
    local display_duration="$2"
  else
    local display_duration="5000"
  fi

  # saves user-set 'display-time' option
  local saved_display_time=$(get_tmux_option "display-time" "750")

  # sets message display time to 5 seconds
  tmux set-option -gq display-time "$display_duration"

  # displays message
  tmux display-message "$message"

  # restores original 'display-time' value
  tmux set-option -gq display-time "$saved_display_time"
}

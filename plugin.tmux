#!/usr/bin/env bash

# shellcheck disable=SC2155
declare -r CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
declare -r FIRST_KEY_TABLE=tmux_make_table_1

set_normal_bindings() {
    tmux bind-key -T prefix "m" switch-client -T "$FIRST_KEY_TABLE"
    tmux bind-key -T "$FIRST_KEY_TABLE" "l" \
        run "tmux split-window -l 10 -c '#{pane_current_path}' $CURRENT_DIR/libexec/ls.tmux '#{pane_id}'"
    tmux bind-key -T "$FIRST_KEY_TABLE" "e" \
        run "tmux display-popup -E -w '75%' -h '75%' $CURRENT_DIR/libexec/edit.tmux '#{pane_id}'"
}

main() {
    set_normal_bindings
}

main
